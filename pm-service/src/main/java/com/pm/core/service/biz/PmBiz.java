package com.pm.core.service.biz;

import com.pm.core.api.PmApi;
import com.pm.core.pojo.dto.PmDto;
import com.pm.core.service.PmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("pmApi")
public class PmBiz implements PmApi {

    @Autowired
    private PmService pmService;

    @Override
    public List<PmDto> pmList() {
        List<PmDto> pmDtoList = pmService.pmList();
        return pmDtoList;
    }
}
