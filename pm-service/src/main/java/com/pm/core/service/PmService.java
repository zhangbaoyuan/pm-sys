package com.pm.core.service;

import com.pm.core.pojo.dto.PmDto;
import org.springframework.stereotype.Service;

import java.util.List;

public interface PmService {

    List<PmDto> pmList();

}
