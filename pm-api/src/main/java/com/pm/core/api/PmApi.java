package com.pm.core.api;

import com.pm.core.pojo.dto.PmDto;

import java.util.List;

public interface PmApi {

    List<PmDto> pmList();
}
