package com.pm.core.service.impl;

import com.pm.core.dao.PmDao;
import com.pm.core.pojo.dto.PmDto;
import com.pm.core.service.PmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("pmService")
public class PmServiceImpl implements PmService {

    @Autowired
    private PmDao pmDao;

    @Override
    public List<PmDto> pmList() {
        List<PmDto> pmDtoList = new ArrayList<>(4);
        PmDto pmDto = new PmDto();
        pmDto.setId(1001L);
        pmDto.setName("Amor");
        pmDtoList.add(pmDto);
        return pmDtoList;
    }
}
